package com.nagarro.advancedjava.assignment.one;

import java.util.Scanner;

public class UserInteraction {
	
	static void introMessage() {
		String flightArr;
		String flightDep;
		String flightDate;
		String flightClass;
		String sortPreference;
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Welcome to Airline search");
		System.out.println("Enter three Character Departure Code (AAA) ");
		flightDep = sc.nextLine();
		System.out.println("Enter three Character Destination Code (AAA) ");
		flightArr = sc.nextLine();
		System.out.println("Enter Date of travel in DD-MM-YYYY format ");
		flightDate = sc.nextLine();
		System.out.println("Enter Class B = Buiseness & E = Economical ");
		flightClass = sc.nextLine();
		System.out.println("Enter Sorting preference F for Fares and T for fare and Time");
		sortPreference = sc.nextLine();
		
		FlightSearch.showFlights(flightDep, flightArr, flightDate, flightClass, sortPreference);
	}

}
