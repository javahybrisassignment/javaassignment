package com.nagarro.advancedjava.assignment.one;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class Flight {
	
	String airlineName;
	String flightNum;
	String flightDep;
	String flightArr;
	Date flightValid;
	String flightTime;
	float flightDur;
	int flightFare;
	String flightSeatAvail;
	String flightClass;
	
	Flight (
			String flightNum,
			String flightDep,
			String flightArr,
			String flightValid,
			String flightTime,
			String flightDur,
			String flightFare,
			String flightSeatAvail,
			String flightClass ) throws Exception{
		
		//Exception handling for departure and arrival missing value
		if(this.flightArr != null) {
			throw new Exception(this.flightNum + " dosen't has Arrival location"); 
		} else if(this.flightDep != null) {
			throw new Exception(this.flightNum + " dosen't has Departure location");
		} else {
			//this.airlineName = airlineName;
			this.flightNum = flightNum;
			this.flightDep = flightDep;
			this.flightArr = flightArr;
			this.flightValid = new SimpleDateFormat("dd-MM-yyyy").parse(flightValid);
			this.flightTime = flightTime;
			this.flightDur = Float.parseFloat(flightDur);
			this.flightFare = Integer.parseInt(flightFare);
			this.flightSeatAvail = flightSeatAvail;
			this.flightClass = flightClass;
		}
	}
	
	int getFare(String flightClass){
		int fares;
		
		if(flightClass.equalsIgnoreCase("B")) {
			fares = (int) (this.flightFare + this.flightFare*(0.4));
		} else {
			fares = this.flightFare;
		}
		
		return fares;
	}
	
	boolean isAvailable(String flightDep, String flightArr, String flightValid, String flightClass) {
		
		boolean flag = false;
		Date dt = null;
		try {
			dt = new SimpleDateFormat("dd-MM-yyyy").parse(flightValid);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		if((this.flightDep.equals(flightDep)) && (this.flightArr.equals(flightArr)) && this.flightClass.contains(flightClass) &&
				(this.flightValid.compareTo(dt)>=0) && this.flightSeatAvail.equalsIgnoreCase("y"))
			flag = true;
		
		return flag;		
	}
	
}

class SortbyFare implements Comparator<Flight> 
{ 
    // Used for sorting in ascending order 
    public int compare(Flight a, Flight b) 
    { 
        return a.flightFare - b.flightFare; 
    } 
} 

class SortbyDuration implements Comparator<Flight> 
{ 
    // Used for sorting in ascending order 
    public int compare(Flight a, Flight b) 
    { 
        return (int) (a.flightDur - b.flightDur); 
    } 
} 
