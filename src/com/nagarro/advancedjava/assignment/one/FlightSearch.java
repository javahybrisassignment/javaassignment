package com.nagarro.advancedjava.assignment.one;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


class FlightSearch {
	
	static
	List <Flight> findFlights(String flightDep, String flightArr, String flightValid, String flightClass) {
		
		List <Flight> flightData = ReadFile.readFile();
		List <Flight> result;
		
		result = flightData.stream().filter(flight -> flight.isAvailable(flightDep, flightArr, flightValid, flightClass))
					.collect(Collectors.toList()); 
		
		return result;
	}
	
	static void showFlights(String flightDep, String flightArr, String flightValid, String flightClass, String sortC) {
		List <Flight> result = findFlights(flightDep, flightArr, flightValid, flightClass);
		
		if(sortC.equals("F")) {
			Collections.sort(result, new SortbyFare());
		} else {
			Collections.sort(result, new SortbyFare()
                    .thenComparing(new SortbyDuration()));
		}
		
		System.out.println("fltNum - Dep - Arr - Time - Dur - Fare - Class");	
		result.forEach(flight->System.out.println(flight.flightNum +" - "+ flight.flightDep +" - "+ flight.flightArr
				+" - "+ flight.flightTime+" - "+flight.flightDur +"hrs - "+flight.getFare(flightClass) +" - "+ flightClass));
	}

}
