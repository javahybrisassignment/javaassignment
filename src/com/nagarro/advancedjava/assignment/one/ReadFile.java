package com.nagarro.advancedjava.assignment.one;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ReadFile {
	
	public static List<Flight> readFile() {
		
		String csvFile;
        String line = "";
        String cvsSplitBy = "\\|";
        
        List <String> files = getFilesFromFolder();
        List <Flight> flightList = new ArrayList<Flight>();
        Iterator<String> itr = files.iterator();
        
        while(itr.hasNext()) {
        	
        	csvFile = "resources\\AirlinesData\\"+itr.next();
        	
        	try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
        		
        		br.readLine();
                while ((line = br.readLine()) != null) {

                    // use pipe as separator
                    String[] data = line.split(cvsSplitBy);  
                    try {
						Flight flightData = new Flight(	data[0],data[1],data[2],data[3], 
													data[4],data[5],data[6],data[7],data[8]);
						flightList.add(flightData);
						
					} catch (Exception e) {
						// prints user defined error in case of dest or Arr location is not set
						e.printStackTrace();
					}
                    
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        return flightList;
    }	
	
	public static List<String> getFilesFromFolder() {
		List<String> filenames = new LinkedList<String>();
		final File folder = new File("resources\\AirlinesData");		
	    for (final File fileEntry : folder.listFiles()) {	      
	    	if(fileEntry.getName().contains(".csv"))
	    		filenames.add(fileEntry.getName());
	    }
	    
	    return filenames;
	}

}
