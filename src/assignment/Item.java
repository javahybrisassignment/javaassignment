package assignment;

public class Item {
	
	String name;
	double price;
	double quantity;
	assignment.Utility.Type type;
	double tax;
	
	Item(String name, double price, double quantity, assignment.Utility.Type type){
		
		this.name = name;
		this.price = price;
		this.quantity = quantity;
		this.type = type;
		
		this.tax = Utility.calculateTax(this.price, this.type);
		
	}
	
	void showDetail(){
		
		double finalPrice;
		
		System.out.println(" Item Name : " + this.name );
		System.out.println(" Item Price : " + this.price );
		System.out.println(" Item Tax : " + this.tax );
		
		finalPrice = this.price + this.tax;
		
		System.out.println(" Item Final Price : " + finalPrice);
		
	}
}
