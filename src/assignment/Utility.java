package assignment;

enum Type {
	RAW, MANUFACTURED, IMPORTED;
}


public class Utility {
	public enum Type {
		RAW, MANUFACTURED, IMPORTED;
	}

	public static double calculateTax(double price, Type type) {
		double tax;
		
		switch (type) {
			case RAW :
				tax = getRawTax(price);
				break;
				
			case MANUFACTURED :
				tax = geManfacturedTax(price);
				break;
				
			case IMPORTED :
				tax = getImportedTax(price);
				break;
				
			default : 
				tax = 0;
		}
		
		return tax;
	}
	
	private static double getRawTax(double price) {
		
		double tax;
		
		tax = price*(12.5/100);
		
		return tax;
	}
	
	private static double geManfacturedTax(double price) {
		
		double tax;
		
		tax = price*(12.5/100) + (price + price*(12.5/100))*(1/50);
		
		return tax;
	}
	
    private static double getImportedTax(double price) {
		
    	double tax;
    	double surcharge = 0;
    	
    	tax = (1/10)*price;
    	
    	if(price + tax <= 100) {
    		surcharge = 5;
    	}else if((price + tax > 100) && (price + tax <= 200)) {
    		surcharge = 10;
    	}else if((price+tax)>200) {
    		surcharge = (5/100)*(price + tax);
    	}
    	
    	tax = tax + surcharge;
    	
		return tax;
	}
    
    
    public static void getUserValue() {
    	
    }
}
