package assignment;
// packages
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class ItemTaxCalculator {
	
	//main method to call user menu 
	// provide user option to choose between adding and viewing item
	public static void main(String[] args) {
		
		// Using Scanner for Getting Input from User 
  		@SuppressWarnings("resource")
		Scanner inp = new Scanner(System.in); 
        int choice = 0;
        
        ArrayList <Item> items = new ArrayList<Item>();
        	//while loop for user menu
		    while (choice < 3) {
		    	System.out.println("____Welcome to item tax calculator____ "); 
		    	System.out.println("___Menu___");
		    	System.out.println(" (1) To add Item "); 
		    	System.out.println(" (2) To Show Items"); 
		    	System.out.println(" (3) To Exit"); 
		    	System.out.println("Enter your choice : ");
		    	choice = inp.nextInt();
		    	inp.nextLine();
		    	
		    	switch (choice) {
		    	case 1:
		    		ItemTaxCalculator.addItems(inp, items);
		    		break;
		    		
		    	case 2:
		    		ItemTaxCalculator.showItems(items);
		    		break;
		    		
		    	case 3: 
		    		System.out.println("Exiting program");
		    		break;
		    		
		    	default:
		    		System.out.println("Invalid choice");
		    	}
		    }
           
        }
        
	
		//addItems Method to add Item to items list
		//Args : Scanner, Arraylist 
        public static void addItems(Scanner inp, ArrayList<Item> li) {
        	String choice = "y";
        	while (choice.equalsIgnoreCase("y")) {
            	
            	System.out.println("____Welcome to item tax calculator____ "); 
            	System.out.println("Enter following details : ");
            	
            	//check is user input is correct
            	try {
            		
            		System.out.println("Item Name : ");
                	String name = inp.nextLine(); 
                    
                	System.out.println("Item Price : ");
                    double price = inp.nextDouble();
                    inp.nextLine();
                    
                    System.out.println("Item Quantity of Item : ");
                    int quantity = inp.nextInt();
                    inp.nextLine();
              
                    System.out.println("Type of Items   "); 
                    System.out.println("1. RAW");
                    System.out.println("2. MANUFACTURED");
                    System.out.println("3. IMPORTED");
                    System.out.println("Please enter a type: "); 
                    String t = inp.nextLine();
                    
                    Utility.Type type = assignment.Utility.Type.valueOf(t); 
                    
                    Item i = new Item(name, price, quantity, type);
                    
                    li.add(i);
                    
            	} catch (Exception e) {
            		
            		// Catch exception 
            		System.out.println("Please enter correct value " + e);
            	
            	} finally {
            		
            		// finally get users choice
            		 System.out.println("You want to enter more item (y/n) : "); 
                     choice = inp.nextLine();
            	}
        	}
        }
        
		//function to display items entered by user
        public static void showItems(ArrayList<Item> li) {
        	Iterator<Item> it = li.iterator(); 
            while (it.hasNext()) {
               ((Item) it.next()).showDetail(); 
               System.out.println("_____________________________________"); 
        	} 
        }
        
}
